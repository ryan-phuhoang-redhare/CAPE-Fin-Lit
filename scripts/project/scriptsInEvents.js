//import { mapSingleElderlyProfiles, mapScenarios, mapScenarioDetails } from "./main.js";
import { arraySingleElderlyProfile, tempSingleElderlyProfileObject,
		arrayScenario, tempScenarioObject,
		arrayDay, tempDayObject,
		arrayOption, tempOptionObject,
		arrayFixedScenario, arrayRandomScenarioAvailable, arrayRandomScenarioDisabled, arrayRecordDayChosenOption } from "./main.js";
import { GameManager } from "./gameManager.js"
import { Scenario } from "./ObjectScenario.js"

export async function InitializeNewGame(runtime)
{
	GameManager.prototype.money = 0;
	GameManager.prototype.happiness = 0;
	GameManager.prototype.gameStatus = "IN_PROGRESS";
	
	runtime.globalVars.currentDayNumber = -1;
	
	arrayRandomScenarioAvailable.splice(0,arrayRandomScenarioAvailable.length);

	arrayRandomScenarioDisabled.splice(0,arrayRandomScenarioDisabled.length);

	arrayRecordDayChosenOption.splice(0,arrayRecordDayChosenOption.length);
	
	await InitializeArrayFixedScenarioAndArrayRandomScenarioAvailable(arrayScenario, arrayFixedScenario, arrayRandomScenarioAvailable);
}

export async function ShowTextValueOnTextObject(textValue, textObject)
{
	//const textInstance = await textObject.getFirstPickedInstance();
	textObject.getFirstPickedInstance().text = textValue;
}

export async function AppendTextValueOnTextObject(textValue, textObject)
{
	//const textInstance = await textObject.getFirstPickedInstance();
	textObject.getFirstPickedInstance().text += textValue;
}

async function LoadToObjectWithKeyAndValue(object, key, value)
{
	object[key] = value;
	//console.log("Key: " + key + ", Value: " + value);
}
async function AddToArrayWithObject(array, object)
{
	const newObject = Object.assign({}, object);
	array.push(newObject);
}

async function FindArrayObjectByKeyAndValue(array, key, value)
{
	const object = array.find(x => x[key] === value);
	return object;
}

async function InitializeArrayFixedScenarioAndArrayRandomScenarioAvailable(arrayAll, arrayFixed, arrayRandom)
{
 	const newArrayFixed = arrayAll.filter(element => element["scenarioType"] === "SPECIFIC_DAY");
 	const newArrayRandom = arrayAll.filter(element => element["scenarioType"] === "STANDALONE_RANDOM");
	
	await newArrayFixed.forEach(element => arrayFixed.push(element));
	await newArrayRandom.forEach(element => arrayRandom.push(element));
	
}

async function LoadScenario(runtime, inputArrayRandomScenarioAvailable, inputArrayRandomScenarioDisabled)
{	
	let currentScenarioObject = new Object();

	if (GameManager.prototype.gameStatus === "OUT_OF_MONEY_AND_HAPPINESS")
	{
		runtime.objects.TextShowingDayNumber.getFirstPickedInstance().isVisible = false;
		runtime.objects.TextShowingMoney.getFirstPickedInstance().isVisible = false;
		runtime.objects.TextShowingHappiness.getFirstPickedInstance().isVisible = false;
		
		const scenarioOutMoneyHappiness = new Scenario("ScenarioOutMoneyHappiness", "Game Over", "SPECIAL", "", "\"You are out of money and happiness.\"", "\"DO YOU WANT TO PLAY AGAIN?\"", "OptionPlayAgainYes", "OptionPlayAgainNo", "", "NO", "\"Think twice before choosing an option, there is no time limit in this game.\"", "0");
		currentScenarioObject = Object.assign({}, scenarioOutMoneyHappiness);
	}
	
	else if (GameManager.prototype.gameStatus === "OUT_OF_MONEY")
	{
		runtime.objects.TextShowingDayNumber.getFirstPickedInstance().isVisible = false;
		runtime.objects.TextShowingMoney.getFirstPickedInstance().isVisible = false;
		runtime.objects.TextShowingHappiness.getFirstPickedInstance().isVisible = false;
		
		const scenarioOutMoney = new Scenario("ScenarioOutMoney", "Game Over", "SPECIAL", "", "\"You have no money left.\"", "\"DO YOU WANT TO PLAY AGAIN?\"", "OptionPlayAgainYes", "OptionPlayAgainNo", "", "NO", "\"Always leave some money in your wallet.\"", "0");
		currentScenarioObject = Object.assign({}, scenarioOutMoney);
	}
	
	else if (GameManager.prototype.gameStatus === "OUT_OF_HAPPINESS")
	{
		runtime.objects.TextShowingDayNumber.getFirstPickedInstance().isVisible = false;
		runtime.objects.TextShowingMoney.getFirstPickedInstance().isVisible = false;
		runtime.objects.TextShowingHappiness.getFirstPickedInstance().isVisible = false;
		
		const scenarioOutHappiness = new Scenario("ScenarioOutHappiness", "Game Over", "SPECIAL", "", "\"You don't have any happiness to continue.\"", "\"DO YOU WANT TO PLAY AGAIN?\"", "OptionPlayAgainYes", "OptionPlayAgainNo", "", "NO", "\"You should take care of your emotion.\"", "0");
		currentScenarioObject = Object.assign({}, scenarioOutHappiness);
	}
	else if (GameManager.prototype.gameStatus === "FINISHED")
	{
		runtime.objects.TextShowingDayNumber.getFirstPickedInstance().isVisible = true;
		runtime.objects.TextShowingMoney.getFirstPickedInstance().isVisible = true;
		runtime.objects.TextShowingHappiness.getFirstPickedInstance().isVisible = true;
		
		const scenarioFinished = new Scenario("ScenarioFinished", "\"Congratulations!\"", "SPECIAL", "", "\"You have finished this game.\"", "\"DO YOU WANT TO PLAY AGAIN?\"", "OptionPlayAgainYes", "OptionPlayAgainNo", "", "NO", "\"You took care of your life very well.\"", "0");
		currentScenarioObject = Object.assign({}, scenarioFinished);
	}
	else //GameManager.prototype.gameStatus === "IN_PROGRESS"
	{
		
		runtime.objects.TextShowingDayNumber.getFirstPickedInstance().isVisible = true;
		runtime.objects.TextShowingMoney.getFirstPickedInstance().isVisible = true;
		runtime.objects.TextShowingHappiness.getFirstPickedInstance().isVisible = true;
		
		runtime.globalVars.currentDayNumber++;
		await ShowTextValueOnTextObject("Day " + runtime.globalVars.currentDayNumber, runtime.objects.TextShowingDayNumber);
		await ShowTextValueOnTextObject("Money: " + GameManager.prototype.money, runtime.objects.TextShowingMoney);
		await ShowTextValueOnTextObject("Happiness: " + GameManager.prototype.happiness, runtime.objects.TextShowingHappiness);

		const dayObject = await FindArrayObjectByKeyAndValue(arrayDay, "dayNumber", runtime.globalVars.currentDayNumber.toString());
		//console.log(dayObject);
		const dayObjectScenarioType = dayObject["dayScenarioType"];
		const dayObjectScenarioIndex = dayObject["dayScenarioIndex"];

		runtime.globalVars.currentDayScenarioType = dayObjectScenarioType;

		if (dayObjectScenarioType === "SPECIFIC_DAY") {
			currentScenarioObject = await FindArrayObjectByKeyAndValue(arrayFixedScenario, "scenarioIndex", dayObjectScenarioIndex);
			//console.log(currentScenarioObject);
		}

		else
		//if (dayObjectScenarioType === "RANDOM_DAY")
		{
			currentScenarioObject = await GetRandomScenario();
		}
	}
	
	await LoadScenarioAndOptionValuesIntoGameView(runtime, currentScenarioObject);
	
	await ManageArrayRandomScenarioAtTheEndOfDay(runtime, currentScenarioObject, inputArrayRandomScenarioAvailable, inputArrayRandomScenarioDisabled);
	
}

async function LoadScenarioAndOptionValuesIntoGameView(runtime, scenarioObject)
{
	const currentScenarioId = scenarioObject["scenarioId"];
	const currentScenarioTitle = scenarioObject["scenarioTitle"];
	//const currentScenarioType = scenarioObject["scenarioType"];
	const currentScenarioSmallText = scenarioObject["scenarioSmallText"];
	const currentScenarioQuestionText = scenarioObject["scenarioQuestionText"];
	const currentScenarioOptionOneId = scenarioObject["scenarioOptionOneId"];
	const currentScenarioOptionTwoId = scenarioObject["scenarioOptionTwoId"];
	const currentScenarioOptionThreeId = scenarioObject["scenarioOptionThreeId"];
	const currentScenarioDidYouKnowText = scenarioObject["scenarioDidYouKnowText"];
	
	await ShowTextValueOnTextObject(currentScenarioTitle,runtime.objects.TextShowingScenarioTitle);
	await ShowTextValueOnTextObject(currentScenarioSmallText.substring(1,currentScenarioSmallText.length-1),runtime.objects.TextShowingScenarioSmallText);
	await ShowTextValueOnTextObject(currentScenarioQuestionText,runtime.objects.TextShowingScenarioQuestionText);
	await ShowTextValueOnTextObject(currentScenarioDidYouKnowText.substring(1,currentScenarioDidYouKnowText.length-1),runtime.objects.TextShowingScenarioDidYouKnowText);
	
	let optionOneObject = new Object();
	let optionOneDescription = "";
	let optionOneFollowUpScenarioId = "";
	
	if (currentScenarioOptionOneId !== "" && currentScenarioOptionOneId !== "OptionPlayAgainYes" && currentScenarioOptionOneId !== "OptionPlayAgainNo")
	{
		optionOneObject = await FindArrayObjectByKeyAndValue(arrayOption, "optionId", currentScenarioOptionOneId);
		optionOneDescription = optionOneObject["optionDescription"];
		optionOneFollowUpScenarioId = optionOneObject["optionFollowUp1"];
		//runtime.globalVars.optionOneOptionId = currentScenarioOptionOneId;
		runtime.globalVars.optionOneFollowUpScenarioId = optionOneFollowUpScenarioId;
	}
	
	else if (currentScenarioOptionOneId === "OptionPlayAgainYes")
	{
		optionOneDescription = "YES";
	}
	
	else if (currentScenarioOptionOneId === "OptionPlayAgainNo")
	{
		optionOneDescription = "NO";
	}
	runtime.globalVars.optionOneOptionId = currentScenarioOptionOneId;
	
	let optionTwoObject = new Object();
	let optionTwoDescription = "";
	let optionTwoFollowUpScenarioId = "";
	
	if (currentScenarioOptionTwoId !== "" && currentScenarioOptionTwoId !== "OptionPlayAgainYes" && currentScenarioOptionTwoId !== "OptionPlayAgainNo")
	{
		optionTwoObject = await FindArrayObjectByKeyAndValue(arrayOption, "optionId", currentScenarioOptionTwoId);
		optionTwoDescription = optionTwoObject["optionDescription"];
		optionTwoFollowUpScenarioId = optionTwoObject["optionFollowUp1"];
// 		runtime.globalVars.optionTwoOptionId = currentScenarioOptionTwoId;
		runtime.globalVars.optionTwoFollowUpScenarioId = optionTwoFollowUpScenarioId;
	}
	
	else if (currentScenarioOptionTwoId === "OptionPlayAgainYes")
	{
		optionTwoDescription = "YES";
	}
	
	else if (currentScenarioOptionTwoId === "OptionPlayAgainNo")
	{
		optionTwoDescription = "NO";
	}
	runtime.globalVars.optionTwoOptionId = currentScenarioOptionTwoId;

	let optionThreeObject = new Object();
	let optionThreeDescription = "";
	let optionThreeFollowUpScenarioId = "";

	runtime.objects.ButtonChooseOptionThree.getFirstPickedInstance().isEnabled = false;
	runtime.objects.ButtonChooseOptionThree.getFirstPickedInstance().isVisible = false;

	if (currentScenarioOptionThreeId !== "" && currentScenarioOptionThreeId !== "OptionPlayAgainYes" && currentScenarioOptionThreeId !== "OptionPlayAgainNo")
	{
		optionThreeObject = await FindArrayObjectByKeyAndValue(arrayOption, "optionId", currentScenarioOptionThreeId);
		optionThreeDescription = optionThreeObject["optionDescription"];
		optionThreeFollowUpScenarioId = optionThreeObject["optionFollowUp1"];
// 		runtime.globalVars.optionThreeOptionId = currentScenarioOptionThreeId;
		runtime.globalVars.optionThreeFollowUpScenarioId = optionThreeFollowUpScenarioId;

		runtime.objects.ButtonChooseOptionThree.getFirstPickedInstance().isEnabled = true;
		runtime.objects.ButtonChooseOptionThree.getFirstPickedInstance().isVisible = true;
	}
	
	else if (currentScenarioOptionThreeId === "OptionPlayAgainYes")
	{
		optionThreeDescription = "YES";
	}
	
	else if (currentScenarioOptionThreeId === "OptionPlayAgainNo")
	{
		optionThreeDescription = "NO";
	}
	runtime.globalVars.optionThreeOptionId = currentScenarioOptionThreeId;
	
	await ShowTextValueOnTextObject(optionOneDescription,runtime.objects.ButtonChooseOptionOne);
	await ShowTextValueOnTextObject(optionTwoDescription,runtime.objects.ButtonChooseOptionTwo);
	await ShowTextValueOnTextObject(optionThreeDescription,runtime.objects.ButtonChooseOptionThree);
	

	runtime.globalVars.currentScenarioId = currentScenarioId;

	const TextLogScenario = "Day " + runtime.globalVars.currentDayNumber + ":" + runtime.globalVars.currentScenarioId + "; ";
	await AppendTextValueOnTextObject(TextLogScenario, runtime.objects.TextLogScenario);

	await ShowTextValueOnTextObject("Random Scenarios Available:\n", runtime.objects.TextLogArrayRandomScenarioAvailable);
	await arrayRandomScenarioAvailable.forEach(element => AppendTextValueOnTextObject(element["scenarioId"]+"; ", runtime.objects.TextLogArrayRandomScenarioAvailable));
	
	//log arrayRecordDayChosenOption
	await ShowTextValueOnTextObject("Days with Options:\n", runtime.objects.TextLogArrayRecordDayChosenOption);
	await arrayRecordDayChosenOption.forEach(element => AppendTextValueOnTextObject("Day " + element["dayNumber"] + ": " + element["optionId"] + "; ", runtime.objects.TextLogArrayRecordDayChosenOption));
}

async function ManageArrayRandomScenarioAtTheEndOfDay(runtime, currentScenarioObject, inputArrayRandomScenarioAvailable, inputArrayRandomScenarioDisabled)
{
	const currentScenarioType = currentScenarioObject["scenarioType"];
	if (currentScenarioType === "SPECIFIC_DAY") {
		//
	}

	else if (currentScenarioType === "STANDALONE_RANDOM")
	{
		await RemoveObjectFromArrayByKeyValue(inputArrayRandomScenarioAvailable, "scenarioId", runtime.globalVars.currentScenarioId);
	}
	else if (currentScenarioType === "FOLLOW_UP_RANDOM")
	{
		//console.log("Scenario Id: "+ currentScenarioId +", Scenario Type: "+ currentScenarioType);
		await RemoveObjectFromArrayByKeyValue(inputArrayRandomScenarioAvailable, "scenarioId", runtime.globalVars.currentScenarioId);
		//await AddToArrayWithObject(inputArrayRandomScenarioDisabled, currentScenarioObject);
	}
}

async function RemoveObjectFromArrayByKeyValue(array, key, value)
{
	const indexToRemove = array.findIndex(x => x[key] === value);
	console.log("Index to remove: " + indexToRemove);
	await array.splice(indexToRemove, 1);
	
	//await array.forEach(element => console.log(element[key]));
}

async function GetRandomScenario()
{	
	const randomIndex = Math.floor(Math.random() * arrayRandomScenarioAvailable.length);
	const randomScenarioObject = arrayRandomScenarioAvailable[randomIndex];
	
	return randomScenarioObject;
}

async function SendGameRecordToServer()
{
	const dataString = JSON.stringify(arrayRecordDayChosenOption);
	console.log(dataString);
	
	var xhr = new XMLHttpRequest();
	var url = "url";
	xhr.open("POST", url, true);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.onreadystatechange = function () {
    	if (xhr.readyState === 4 && xhr.status === 200) {
        	var json = JSON.parse(xhr.responseText);
			console.log(json);
    	}
	};
	xhr.send(dataString);
}

async function OnClickButtonOption(runtime, chosenOptionId)
{
	if (chosenOptionId === "OptionPlayAgainYes")
	{
		console.log("Yes Play Again");
		await SendGameRecordToServer();
		await InitializeNewGame(runtime);
		await LoadScenario(runtime, arrayRandomScenarioAvailable, arrayRandomScenarioDisabled);
		await ShowTextValueOnTextObject("Scenarios:\n",runtime.objects.TextLogScenario);
	}
	
	else if (chosenOptionId === "OptionPlayAgainNo")
	{
		console.log("No Don't Play Again");
		await SendGameRecordToServer();
		await InitializeNewGame(runtime);
		runtime.goToLayout("Layout 0");
	}
	
	else //if (optionId !== "OptionPlayAgainYes") && (optionId !== "OptionPlayAgainNo")
	{
		const chosenOptionObject = await FindArrayObjectByKeyAndValue(arrayOption, "optionId", chosenOptionId);
		const optionAddedMoney = chosenOptionObject["optionMoney"];
		await GameManager.prototype.addMoney(optionAddedMoney);

		const optionAddedHappiness = chosenOptionObject["optionHappiness"];
		await GameManager.prototype.addHappiness(optionAddedHappiness);
		
		await UpdateGameManagerGameStatus();
		
		if (GameManager.prototype.gameStatus === "IN_PROGRESS")
		{
			//Create an array to contain all the follow up scenario ids of the chosen option
			let arrayFollowUpScenarioIdFromChosenOption = new Array();
			
			const followUpScenarioIdOne = chosenOptionObject["optionFollowUp1"];
			const followUpScenarioIdTwo = chosenOptionObject["optionFollowUp2"];
			const followUpScenarioIdThree = chosenOptionObject["optionFollowUp3"];
			
			//Now the array has all the follow up scenario ids, including empty ones
			await AddToArrayWithObject(arrayFollowUpScenarioIdFromChosenOption, followUpScenarioIdOne);
			await AddToArrayWithObject(arrayFollowUpScenarioIdFromChosenOption, followUpScenarioIdTwo);
			await AddToArrayWithObject(arrayFollowUpScenarioIdFromChosenOption, followUpScenarioIdThree);
			
			//Check reusable status for each follow up scenario id
			//await arrayFollowUpScenarioIdFromChosenOption.forEach(element => );
			
			
		}
		
		const objectRecordDayChosenOption = { "dayNumber": runtime.globalVars.currentDayNumber.toString(), "optionId": runtime.globalVars.optionThreeOptionId};
		await AddToArrayWithObject(arrayRecordDayChosenOption, objectRecordDayChosenOption);

		await LoadScenario(runtime, arrayRandomScenarioAvailable, arrayRandomScenarioDisabled);
	}
}

async function UpdateGameManagerGameStatus()
{
	if (GameManager.prototype.money < 0 && GameManager.prototype.happiness < 0)
	{
		GameManager.prototype.gameStatus = "OUT_OF_MONEY_AND_HAPPINESS";
	}
	else if (GameManager.prototype.money < 0 && GameManager.prototype.happiness >= 0)
	{
		GameManager.prototype.gameStatus = "OUT_OF_MONEY";
	}
	else if (GameManager.prototype.money >= 0 && GameManager.prototype.happiness < 0)
	{
		GameManager.prototype.gameStatus = "OUT_OF_HAPPINESS";
	}
	else //both money and happiness >= 0 
	{
		if (runtime.globalVars.currentDayNumber >= arrayDay.length-1)
		{
			GameManager.prototype.gameStatus = "FINISHED";
		}
		else //currentDayNumber < arrayDay.length
		{
			GameManager.prototype.gameStatus = "IN_PROGRESS";
		}
}

// await function CheckValidityAndAddFollowUpToRandomList(followUpScenarioId)
// {
// 	//First check if the chosen option has a follow up scenario id at this element or not
// 	if (followUpScenarioId !== "")
// 	{
// 		//If there is a follow up scenario id, check if the id has been found before and disabled or not
// 		const foundObjectInArrayRandomScenarioDisabled = await FindArrayObjectByKeyAndValue(arrayRandomScenarioDisabled, "scenarioId", runtime.globalVars.optionThreeFollowUpScenarioId);
// 		if(!foundObjectInArrayRandomScenarioDisabled)
// 		{
// 			//The id is not found in the disabled list, which means this follow up scenario id is just first time found
// 			const scenarioObject = await FindArrayObjectByKeyAndValue(arrayScenario, "scenarioId", runtime.globalVars.optionThreeFollowUpScenarioId);
// 			//So we add the scenario into the available random list, at the same time we also disable it so that it won't appear again in the current game session
// 			await AddToArrayWithObject(arrayRandomScenarioAvailable, scenarioObject);
// 			await AddToArrayWithObject(arrayRandomScenarioDisabled, scenarioObject);
// 		}

// 	}
}


const scriptsInEvents = {

	async EventSheet1_Event1_Act1(runtime, localVars)
	{
		await LoadToObjectWithKeyAndValue(tempSingleElderlyProfileObject,"profileId","profile0001");
	},

	async EventSheet1_Event3_Act2(runtime, localVars)
	{
		await LoadToObjectWithKeyAndValue(tempScenarioObject,"scenarioId",localVars.tempValueToLoadToObject)
	},

	async EventSheet1_Event3_Act4(runtime, localVars)
	{
		await LoadToObjectWithKeyAndValue(tempScenarioObject,"scenarioTitle",localVars.tempValueToLoadToObject)
	},

	async EventSheet1_Event3_Act6(runtime, localVars)
	{
		await LoadToObjectWithKeyAndValue(tempScenarioObject,"scenarioType",localVars.tempValueToLoadToObject);
	},

	async EventSheet1_Event3_Act8(runtime, localVars)
	{
		await LoadToObjectWithKeyAndValue(tempScenarioObject,"scenarioIndex",localVars.tempValueToLoadToObject);
	},

	async EventSheet1_Event3_Act10(runtime, localVars)
	{
		await LoadToObjectWithKeyAndValue(tempScenarioObject,"scenarioSmallText",localVars.tempValueToLoadToObject);
	},

	async EventSheet1_Event3_Act12(runtime, localVars)
	{
		await LoadToObjectWithKeyAndValue(tempScenarioObject,"scenarioQuestionText",localVars.tempValueToLoadToObject);
	},

	async EventSheet1_Event3_Act14(runtime, localVars)
	{
		await LoadToObjectWithKeyAndValue(tempScenarioObject,"scenarioOptionOneId",localVars.tempValueToLoadToObject);
	},

	async EventSheet1_Event3_Act16(runtime, localVars)
	{
		await LoadToObjectWithKeyAndValue(tempScenarioObject,"scenarioOptionTwoId",localVars.tempValueToLoadToObject);
	},

	async EventSheet1_Event3_Act18(runtime, localVars)
	{
		await LoadToObjectWithKeyAndValue(tempScenarioObject,"scenarioOptionThreeId",localVars.tempValueToLoadToObject);
	},

	async EventSheet1_Event3_Act20(runtime, localVars)
	{
		await LoadToObjectWithKeyAndValue(tempScenarioObject,"scenarioWillShowStats",localVars.tempValueToLoadToObject);
	},

	async EventSheet1_Event3_Act22(runtime, localVars)
	{
		await LoadToObjectWithKeyAndValue(tempScenarioObject,"scenarioDidYouKnowText",localVars.tempValueToLoadToObject);
	},

	async EventSheet1_Event3_Act24(runtime, localVars)
	{
		await LoadToObjectWithKeyAndValue(tempScenarioObject,"scenarioState",localVars.tempValueToLoadToObject);
	},

	async EventSheet1_Event3_Act25(runtime, localVars)
	{
		await AddToArrayWithObject(arrayScenario, tempScenarioObject);
		//tempScenarioObject = {};
	},

	async EventSheet1_Event4_Act1(runtime, localVars)
	{
		await LoadToObjectWithKeyAndValue(tempSingleElderlyProfileObject,"arrayScenario",arrayScenario);
		await InitializeArrayFixedScenarioAndArrayRandomScenarioAvailable(arrayScenario, arrayFixedScenario, arrayRandomScenarioAvailable);
		
		//await arrayFixedScenario.forEach(element => console.log("Fixed: " + element["scenarioId"]));
		//await arrayRandomScenarioAvailable.forEach(element => console.log("Random: " + element["scenarioId"]));
	},

	async EventSheet1_Event6_Act2(runtime, localVars)
	{
		await LoadToObjectWithKeyAndValue(tempDayObject,"dayNumber",localVars.tempValueToLoadToObject)
	},

	async EventSheet1_Event6_Act4(runtime, localVars)
	{
		await LoadToObjectWithKeyAndValue(tempDayObject,"dayScenarioType",localVars.tempValueToLoadToObject)
	},

	async EventSheet1_Event6_Act6(runtime, localVars)
	{
		await LoadToObjectWithKeyAndValue(tempDayObject,"dayScenarioIndex",localVars.tempValueToLoadToObject);
	},

	async EventSheet1_Event6_Act7(runtime, localVars)
	{
		await AddToArrayWithObject(arrayDay,tempDayObject);
		//tempDayObject = {};
	},

	async EventSheet1_Event7_Act1(runtime, localVars)
	{
		await LoadToObjectWithKeyAndValue(tempSingleElderlyProfileObject,"arrayDay",arrayDay);
	},

	async EventSheet1_Event9_Act2(runtime, localVars)
	{
		await LoadToObjectWithKeyAndValue(tempOptionObject,"optionId",localVars.tempValueToLoadToObject)
	},

	async EventSheet1_Event9_Act4(runtime, localVars)
	{
		await LoadToObjectWithKeyAndValue(tempOptionObject,"optionDescription",localVars.tempValueToLoadToObject)
	},

	async EventSheet1_Event9_Act6(runtime, localVars)
	{
		await LoadToObjectWithKeyAndValue(tempOptionObject,"optionMoney",localVars.tempValueToLoadToObject)
	},

	async EventSheet1_Event9_Act8(runtime, localVars)
	{
		await LoadToObjectWithKeyAndValue(tempOptionObject,"optionHappiness",localVars.tempValueToLoadToObject);
	},

	async EventSheet1_Event9_Act10(runtime, localVars)
	{
		await LoadToObjectWithKeyAndValue(tempOptionObject,"optionFollowUp1",localVars.tempValueToLoadToObject)
	},

	async EventSheet1_Event9_Act12(runtime, localVars)
	{
		await LoadToObjectWithKeyAndValue(tempOptionObject,"optionFollowUp2",localVars.tempValueToLoadToObject)
	},

	async EventSheet1_Event9_Act14(runtime, localVars)
	{
		await LoadToObjectWithKeyAndValue(tempOptionObject,"optionFollowUp3",localVars.tempValueToLoadToObject);
	},

	async EventSheet1_Event9_Act15(runtime, localVars)
	{
		await AddToArrayWithObject(arrayOption,tempOptionObject);
		//tempOptionObject = {};
	},

	async EventSheet1_Event10_Act1(runtime, localVars)
	{
		await LoadToObjectWithKeyAndValue(tempSingleElderlyProfileObject, "arrayOption", arrayOption);
	},

	async EventSheet1_Event10_Act2(runtime, localVars)
	{
		await AddToArrayWithObject(arraySingleElderlyProfile, tempSingleElderlyProfileObject);
		//tempSingleElderlyProfileObject = {};
	},

	async EventSheet1_Event10_Act3(runtime, localVars)
	{
		await LoadScenario(runtime, arrayRandomScenarioAvailable, arrayRandomScenarioDisabled);
	},

	async EventSheet1_Event11_Act1(runtime, localVars)
	{
		const foundSingleElderlyProfile = await FindArrayObjectByKeyAndValue(arraySingleElderlyProfile,"profileId", await runtime.objects.TextInput1.getFirstPickedInstance().text);
		const foundScenario = await FindArrayObjectByKeyAndValue(foundSingleElderlyProfile["arrayScenario"],"scenarioId", await runtime.objects.TextInput2.getFirstPickedInstance().text);
		const valueToShowOnTextBox = foundScenario[await runtime.objects.TextInput3.getFirstPickedInstance().text];
		await ShowTextValueOnTextObject(valueToShowOnTextBox, runtime.objects.TextShowingMapValue);
	},

	async EventSheet1_Event12_Act1(runtime, localVars)
	{
		const foundSingleElderlyProfile = await FindArrayObjectByKeyAndValue(arraySingleElderlyProfile, "profileId", await runtime.objects.TextInput1.getFirstPickedInstance().text);
		const foundDay = await FindArrayObjectByKeyAndValue(foundSingleElderlyProfile["arrayDay"], "dayNumber", await runtime.objects.TextInput2.getFirstPickedInstance().text);
		const valueToShowOnTextBox = foundDay[await runtime.objects.TextInput3.getFirstPickedInstance().text];
		await ShowTextValueOnTextObject(valueToShowOnTextBox, runtime.objects.TextShowingMapValue);
	},

	async EventSheet1_Event13_Act1(runtime, localVars)
	{
		const foundSingleElderlyProfile = await FindArrayObjectByKeyAndValue(arraySingleElderlyProfile,"profileId",await runtime.objects.TextInput1.getFirstPickedInstance().text);
		const foundOption = await FindArrayObjectByKeyAndValue(foundSingleElderlyProfile["arrayOption"],"optionId",await runtime.objects.TextInput2.getFirstPickedInstance().text);
		const valueToShowOnTextBox = foundOption[await runtime.objects.TextInput3.getFirstPickedInstance().text];
		await ShowTextValueOnTextObject(valueToShowOnTextBox, runtime.objects.TextShowingMapValue);
	},

	async EventSheet1_Event14_Act1(runtime, localVars)
	{
		if (runtime.globalVars.optionOneOptionId === "OptionPlayAgainYes")
		{
			console.log("Yes Play Again");
			await SendGameRecordToServer();
			await InitializeNewGame(runtime);
		 	await LoadScenario(runtime, arrayRandomScenarioAvailable, arrayRandomScenarioDisabled);
			await ShowTextValueOnTextObject("Scenarios:\n",runtime.objects.TextLogScenario);
			return;
		}
		
		const objectRecordDayChosenOption = { "dayNumber": runtime.globalVars.currentDayNumber.toString(), "optionId": runtime.globalVars.optionOneOptionId};
		await AddToArrayWithObject(arrayRecordDayChosenOption, objectRecordDayChosenOption);
		
		const chosenOptionObject = await FindArrayObjectByKeyAndValue(arrayOption, "optionId", runtime.globalVars.optionOneOptionId);
		const optionAddedMoney = chosenOptionObject["optionMoney"];
		await GameManager.prototype.addMoney(optionAddedMoney);
		
		const optionAddedHappiness = chosenOptionObject["optionHappiness"];
		await GameManager.prototype.addHappiness(optionAddedHappiness);
		
		if (GameManager.prototype.money < 0 && GameManager.prototype.happiness < 0)
		{
			GameManager.prototype.gameStatus = "OUT_OF_MONEY_AND_HAPPINESS";
		}
		else if (GameManager.prototype.money < 0 && GameManager.prototype.happiness >= 0)
		{
			GameManager.prototype.gameStatus = "OUT_OF_MONEY";
		}
		else if (GameManager.prototype.money >= 0 && GameManager.prototype.happiness < 0)
		{
			GameManager.prototype.gameStatus = "OUT_OF_HAPPINESS";
		}
		else //both money and happimess >= 0 
		{
			if (runtime.globalVars.currentDayNumber >= arrayDay.length-1)
			{
				GameManager.prototype.gameStatus = "FINISHED";
			}
			else //currentDayNumber < arrayDay.length
			{
				if (runtime.globalVars.optionOneFollowUpScenarioId !== "")
				{
					const foundObjectInArrayRandomScenarioAvailable = await FindArrayObjectByKeyAndValue(arrayRandomScenarioAvailable, "scenarioId", runtime.globalVars.optionOneFollowUpScenarioId);
					if(!foundObjectInArrayRandomScenarioAvailable)
					{
						const foundObjectInArrayRandomScenarioDisabled = await FindArrayObjectByKeyAndValue(arrayRandomScenarioDisabled, "scenarioId", runtime.globalVars.optionOneFollowUpScenarioId);
						if(!foundObjectInArrayRandomScenarioDisabled)
						{
							const scenarioObject = await FindArrayObjectByKeyAndValue(arrayScenario, "scenarioId", runtime.globalVars.optionOneFollowUpScenarioId);
							await AddToArrayWithObject(arrayRandomScenarioAvailable, scenarioObject);
						}
					}
				}
			}
		}
		
		await LoadScenario(runtime, arrayRandomScenarioAvailable, arrayRandomScenarioDisabled);
	},

	async EventSheet1_Event15_Act1(runtime, localVars)
	{
		if (runtime.globalVars.optionTwoOptionId === "OptionPlayAgainNo")
		{
			console.log("No Don't Play Again");
			await SendGameRecordToServer();
			await InitializeNewGame(runtime);
		 	runtime.goToLayout("Layout 0");
			return;
		}
		
		const objectRecordDayChosenOption = { "dayNumber": runtime.globalVars.currentDayNumber.toString(), "optionId": runtime.globalVars.optionTwoOptionId};
		await AddToArrayWithObject(arrayRecordDayChosenOption, objectRecordDayChosenOption);
		
		const chosenOptionObject = await FindArrayObjectByKeyAndValue(arrayOption, "optionId", runtime.globalVars.optionTwoOptionId);
		const optionAddedMoney = chosenOptionObject["optionMoney"];
		await GameManager.prototype.addMoney(optionAddedMoney);
		
		const optionAddedHappiness = chosenOptionObject["optionHappiness"];
		await GameManager.prototype.addHappiness(optionAddedHappiness);
		
		if (GameManager.prototype.money < 0 && GameManager.prototype.happiness < 0)
		{
			GameManager.prototype.gameStatus = "OUT_OF_MONEY_AND_HAPPINESS";
		}
		else if (GameManager.prototype.money < 0 && GameManager.prototype.happiness >= 0)
		{
			GameManager.prototype.gameStatus = "OUT_OF_MONEY";
		}
		else if (GameManager.prototype.money >= 0 && GameManager.prototype.happiness < 0)
		{
			GameManager.prototype.gameStatus = "OUT_OF_HAPPINESS";
		}
		else //both money and happimess >= 0 
		{
			if (runtime.globalVars.currentDayNumber >= arrayDay.length-1)
			{
				GameManager.prototype.gameStatus = "FINISHED";
			}
			else //currentDayNumber < arrayDay.length
			{
				if (runtime.globalVars.optionTwoFollowUpScenarioId !== "")
				{
					const foundObjectInArrayRandomScenarioAvailable = await FindArrayObjectByKeyAndValue(arrayRandomScenarioAvailable, "scenarioId", runtime.globalVars.optionTwoFollowUpScenarioId);
					if(!foundObjectInArrayRandomScenarioAvailable)
					{
						const foundObjectInArrayRandomScenarioDisabled = await FindArrayObjectByKeyAndValue(arrayRandomScenarioDisabled, "scenarioId", runtime.globalVars.optionTwoFollowUpScenarioId);
						if(!foundObjectInArrayRandomScenarioDisabled)
						{
							const scenarioObject = await FindArrayObjectByKeyAndValue(arrayScenario, "scenarioId", runtime.globalVars.optionTwoFollowUpScenarioId);
							await AddToArrayWithObject(arrayRandomScenarioAvailable, scenarioObject);
						}
					}
				}
			}
		}
		
		await LoadScenario(runtime, arrayRandomScenarioAvailable, arrayRandomScenarioDisabled);
	},

	async EventSheet1_Event16_Act1(runtime, localVars)
	{
		const objectRecordDayChosenOption = { "dayNumber": runtime.globalVars.currentDayNumber.toString(), "optionId": runtime.globalVars.optionThreeOptionId};
		await AddToArrayWithObject(arrayRecordDayChosenOption, objectRecordDayChosenOption);
		
		const chosenOptionObject = await FindArrayObjectByKeyAndValue(arrayOption, "optionId", runtime.globalVars.optionThreeOptionId);
		const optionAddedMoney = chosenOptionObject["optionMoney"];
		await GameManager.prototype.addMoney(optionAddedMoney);
		
		const optionAddedHappiness = chosenOptionObject["optionHappiness"];
		await GameManager.prototype.addHappiness(optionAddedHappiness);
		
		if (GameManager.prototype.money < 0 && GameManager.prototype.happiness < 0)
		{
			GameManager.prototype.gameStatus = "OUT_OF_MONEY_AND_HAPPINESS";
		}
		else if (GameManager.prototype.money < 0 && GameManager.prototype.happiness >= 0)
		{
			GameManager.prototype.gameStatus = "OUT_OF_MONEY";
		}
		else if (GameManager.prototype.money >= 0 && GameManager.prototype.happiness < 0)
		{
			GameManager.prototype.gameStatus = "OUT_OF_HAPPINESS";
		}
		else //both money and happimess >= 0 
		{
			if (runtime.globalVars.currentDayNumber >= arrayDay.length-1)
			{
				GameManager.prototype.gameStatus = "FINISHED";
			}
			else //currentDayNumber < arrayDay.length
			{
				if (runtime.globalVars.optionThreeFollowUpScenarioId !== "")
				{
					const foundObjectInArrayRandomScenarioAvailable = await FindArrayObjectByKeyAndValue(arrayRandomScenarioAvailable, "scenarioId", runtime.globalVars.optionThreeFollowUpScenarioId);
					if(!foundObjectInArrayRandomScenarioAvailable)
					{
						const foundObjectInArrayRandomScenarioDisabled = await FindArrayObjectByKeyAndValue(arrayRandomScenarioDisabled, "scenarioId", runtime.globalVars.optionThreeFollowUpScenarioId);
						if(!foundObjectInArrayRandomScenarioDisabled)
						{
							const scenarioObject = await FindArrayObjectByKeyAndValue(arrayScenario, "scenarioId", runtime.globalVars.optionThreeFollowUpScenarioId);
							await AddToArrayWithObject(arrayRandomScenarioAvailable, scenarioObject);
						}
					}
				}
			}
		}
		
		await LoadScenario(runtime, arrayRandomScenarioAvailable, arrayRandomScenarioDisabled);
	}

};

self.C3.ScriptsInEvents = scriptsInEvents;

