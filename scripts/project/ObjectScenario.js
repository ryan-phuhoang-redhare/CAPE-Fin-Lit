export class Scenario {
  constructor(scenarioId, scenarioTitle, scenarioType, scenarioIndex, scenarioSmallText, scenarioQuestionText, scenarioOptionOneId, scenarioOptionTwoId, scenarioOptionThreeId, scenarioWillShowStats, scenarioDidYouKnowText, scenarioState) {
    this.scenarioId = scenarioId;
    this.scenarioTitle = scenarioTitle;
	this.scenarioType = scenarioType;
	this.scenarioIndex = scenarioIndex;
    this.scenarioSmallText = scenarioSmallText;
	this.scenarioQuestionText = scenarioQuestionText;
	this.scenarioOptionOneId = scenarioOptionOneId;
    this.scenarioOptionTwoId = scenarioOptionTwoId;
	this.scenarioOptionThreeId = scenarioOptionThreeId;
	this.scenarioWillShowStats = scenarioWillShowStats;
    this.scenarioDidYouKnowText = scenarioDidYouKnowText;
	this.scenarioState = scenarioState;
  }
  
  getScenarioId()
  {
  	return this.scenarioId;
  }
  getScenarioTitle()
  {
  	return this.scenarioTitle;
  }
  getScenarioType()
  {
  	return this.scenarioType;
  }
  getScenarioIndex()
  {
  	return this.scenarioIndex;
  }
  getScenarioSmallText()
  {
  	return this.scenarioSmallText;
  }
  getScenarioQuestionText()
  {
  	return this.scenarioQuestionText;
  }
  getScenarioOptionOneId()
  {
  	return this.scenarioOptionOneId;
  }
  getScenarioOptionTwoId()
  {
  	return this.scenarioOptionTwoId;
  }
  getScenarioOptionThreeId()
  {
  	return this.scenarioOptionThreeId;
  }
  getScenarioWillShowStats()
  {
  	return this.scenarioWillShowStats;
  }
  getScenarioDidYouKnowText()
  {
  	return this.scenarioDidYouKnowText;
  }
  getScenarioState()
  {
  	return this.scenarioState;
  }
}