export class GameManager {
  constructor(currentDayNumber, money, happiness, gameStatus) {
  	this.currentDayNumber = currentDayNumber;
    this.money = money;
    this.happiness = happiness;
	this.gameStatus = gameStatus;
  }
  
  getCurrentDayNumber()
  {
  	return this.currentDayNumber;
  }
  
  getMoney()
  {
  	return this.money;
  }
  
  getHappiness()
  {
  	return this.happiness;
  }
  
  getGameStatus()
  {
  	return this.gameStatus;
  }
  
  addMoney(addedMoney)
  {
  	this.money += Number(addedMoney);
  }
  
  addHappiness(addedHappiness)
  {
  	this.happiness += Number(addedHappiness);
  }
}