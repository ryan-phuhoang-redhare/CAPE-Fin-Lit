
// Import any other script files here, e.g.:
// import * as myModule from "./mymodule.js";
// import { GameManager } from "./gameManager.js";
import { InitializeNewGame } from "./importsForEvents.js";

// export let mapSingleElderlyProfiles = new Map();
// export let mapScenarios = new Map();
// export let mapScenarioDetails = new Map();
export let arraySingleElderlyProfile = new Array();
export let tempSingleElderlyProfileObject = new Object();

export let arrayScenario = new Array();
export let tempScenarioObject = new Object();

export let arrayDay = new Array();
export let tempDayObject = new Object();

export let arrayOption = new Array();
export let tempOptionObject = new Object();

export let arrayFixedScenario = new Array();

export let arrayRandomScenarioAvailable = new Array();

export let arrayRandomScenarioDisabled = new Array();

export let arrayRecordDayChosenOption = new Array();

runOnStartup(async runtime =>
{
	// Code to run on the loading screen.
	// Note layouts, objects etc. are not yet available.
	runtime.addEventListener("beforeprojectstart", () => OnBeforeProjectStart(runtime));
	
	await InitializeNewGame(runtime);
});

async function OnBeforeProjectStart(runtime)
{
	// Code to run just before 'On start of layout' on
	// the first layout. Loading has finished and initial
	// instances are created and available to use here.
	runtime.addEventListener("tick", () => Tick(runtime));

// 	const textFromCsvSingleElderlyProfile = await GetContentFromCsvName(runtime, "CAPE Fin Lit Scenarios - Single_Elderly_Profile.csv");
// 	console.log(textFromCsvSingleElderlyProfile);
//  	const layoutObjectTextSingleElderlyProfile =  runtime.objects.TextFromCSVSingleElderlyProfile;
//  	await ShowTextValueOnTextObject(textFromCsvSingleElderlyProfile, layoutObjectTextSingleElderlyProfile);
	
// 	const textFromCsvDay = await GetContentFromCsvName(runtime, "CAPE Fin Lit Scenarios - Day.csv");
// 	console.log(textFromCsvDay);
//  	const layoutObjectTextDay =  runtime.objects.TextFromCSVDay;
//  	await ShowTextValueOnTextObject(textFromCsvDay, layoutObjectTextDay);
	
// 	const textFromCsvOptionsId = await GetContentFromCsvName(runtime, "CAPE Fin Lit Scenarios - Options_ID.csv");
// 	console.log(textFromCsvOptionsId);
//  	const layoutObjectTextOptionsId =  runtime.objects.TextFromCSVOptionsID;
//  	await ShowTextValueOnTextObject(textFromCsvOptionsId, layoutObjectTextOptionsId);
	
}

function Tick(runtime)
{
	// Code to run every tick
	
}

// async function GetContentFromCsvName(runtime, csvFileName)
// {
// 	const csvFileUrl = await runtime.assets.getProjectFileUrl(csvFileName);
// 	const csvResponse = await fetch(csvFileUrl);
// 	const fetchedCsvText = await csvResponse.text();
// 	return fetchedCsvText;
// }